package fakecompany.com.myapplication30


import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_second.*


class SecondActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)
        /**
         * add button listener that launch the introduction pages
         */
        button0.setOnClickListener{
            val intent = Intent(this,SanwichActivity::class.java)
            startActivity(intent)
        }
    }
}